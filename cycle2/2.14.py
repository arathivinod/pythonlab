print("Enter a list of integers separated by commas: ")
list = [int(i) for i in input().split(',')] 
for i in list: 
        if i % 2 == 0: 
            list.remove(i) 
print("List of integers after removing even numbers: ",list)
