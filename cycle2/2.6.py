print("Enter a list of numbers to generate positive list seperated by spaces: ")
list = [int(i) for i in input().split()] 
l1=[i for i in list if i>0]
print("Positive list: ",l1) 

print("Enter a list of numbers to generate square list seperated by spaces: ")
list = [int(i) for i in input().split()]
l2=[i**2 for i in list] 
print("Square list: ",l2) 

print("Enter a word to generate vowel list: ")
word = input() 
l3=[i for i in word if i in "aeiouAEIOU"]
print("Vowel list: ",l3) 

print("Enter a word to generate ordinal list: ")
word = input() 
l4=[ord(i) for i in word]
print("Ordinal list: ",l4) 
